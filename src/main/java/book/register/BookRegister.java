package book.register;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Iterator;
import java.util.Scanner;


/**
 * A book register for organizing and maintaining NTNU's library.
 *
 * @author  Tomas Klungerbo Olsen
 * @version 0.1
 * @since   0.1
 */
public class BookRegister
{
  private ArrayList<Book> books;

  public BookRegister()
  {
    this.books = new ArrayList<Book>();
  }

  /**
   * Initializes the book register by loading books.
   */
  public void initialize()
  {
    this.loadBooks();
  }


  /**
   * Adds a book to the book register.
   *
   * @param book The book to be added.
   */
  public void addBook(Book book)
  {
    this.books.add(book);
  }


  /**
   * Remove a book by its index.
   *
   * @param index The index of the book to remove.
   */
  public void removeBookByIndex(int index)
  {
    System.out.println("--> Removing book at index " + index);

    try
    {
      if (this.books.get(index) != null)
      {
        this.books.remove(index);
        System.out.println("The book was successfully removed from the registry");
        System.out.println();
      }
    }
    catch(IndexOutOfBoundsException e)
    {
      System.out.println("Could not find a book at index " + index);
      System.out.println();
    }
  }


  /**
   * Removes a book based on a book object reference.
   *
   * @param book The book object reference of the book to be removed.
   */
  public void removeBook(Book book)
  {
    try
    {
      System.out.println("--> Removing book:");
      book.printInformation();
      System.out.println("");

      boolean removed = this.books.remove(book);

      if (removed)
      {
        System.out.println("The book was successfully removed!");
        System.out.println();
      }
      else
      {
        System.out.print("Could not find that book in the registry.");
        System.out.println();
      }
    }
    catch (NullPointerException e)
    {
      System.out.print("The book has not been constructed properly");
    }
  }


  /**
   * Removes a book by its title.
   *
   * @param title The title of the book to be removed.
   */
  public void removeBookByTitle(String title)
  {
    System.out.println("--> Removing book with title: " + title);
    ArrayList<Book> booksWithTitle = this.getBooksByTitle(title);

    if (booksWithTitle != null) 
    {
      Book bookToRemove = this.selectBookFromCollection(booksWithTitle);
      this.books.remove(bookToRemove);
      System.out.println("The book was successfully removed!");
      System.out.println();
    }
    else
    {
      System.out.println("Could not find any books with that title.");
      System.out.println();
    }
  }


  /**
  * Select a book from a collection of books.
  * If there is only one book in the collection it is automaticly selected.
  *
  * @param booksCollection The book collection to select from.
  * @return The book chosen from the collection.
  */
  private Book selectBookFromCollection(ArrayList<Book> booksCollection)
  {
    Book bookChosen = null;

    if (booksCollection.size() == 1)
    {
      bookChosen = booksCollection.get(0);
    }
    else
    {
      int choice;
      Scanner input = new Scanner(System.in);

      do
      {
        System.out.println("There are multiple books to choose from:");

        for (int i = 0; i < booksCollection.size(); i++)
        {
          final int bookNumber = i + 1;
          System.out.println("[" + bookNumber + "]");
          booksCollection.get(i).printInformation();
          System.out.println("");
        }

        System.out.print("Please choose a book [1-" + booksCollection.size() + "]: ");
        choice = input.nextInt();
        System.out.println("");

      } while (choice < 1 || choice > (booksCollection.size()));

      final int bookChosenIndex = choice - 1;
      bookChosen = booksCollection.get(bookChosenIndex);
    }

    return bookChosen;
  }


  /**
   * Remove a book by its EAN-13 barcode.
   *
   * @param ean13Barcode The EAN-13 barcode of the book to be removed.
   */
  public void removeBookByEAN13Barcode(long ean13Barcode)
  {
    System.out.println("--> Removing book with EAN-13 barcode: " + ean13Barcode);
    ArrayList<Book> booksWithEAN13 = this.getBooksByEAN13Barcode(ean13Barcode);

    if (booksWithEAN13 != null)
    {
      Book bookToRemove = this.selectBookFromCollection(booksWithEAN13);
      this.books.remove(bookToRemove);
      System.out.println("The book was successfully removed!");
      System.out.println();
    }
    else
    {
      System.out.println("Could not find a book with that EAN-13 barcode.");
      System.out.println();
    }
  }


  /**
   * Print all books to the console
   */
  public void printAllBooks()
  {
    for (Book book : this.books)
    {
      book.printInformation();
      System.out.println("");
    }
  }


  /**
   * Find all books by a given author.
   *
   * @param author The author of the books to search for.
   * @return The books by the given author or null if none.
   */
  public ArrayList<Book> getBooksByAuthor(String author)
  {
    return this.books.parallelStream()
      .filter(book -> author.equals(book.getAuthor()))
      .collect(Collectors.toCollection(ArrayList::new));
  }


  /**
   * Find all books by its EAN-13 barcode
   *
   * @param ean13Barcode The barcode of the books to search for.
   * @return The books with the EAN-13 bar code or null if none.
   */
  public ArrayList<Book> getBooksByEAN13Barcode(long ean13Barcode)
  {
    return this.books.parallelStream()
      .filter(book -> ean13Barcode == book.getBarcodeEAN13())
      .collect(Collectors.toCollection(ArrayList::new));
  }


  /**
   * Returns all books by the given title.
   *
   * @param title The title of the books to look for.
   * @return The books that was found or null if none.
   */
  public ArrayList<Book> getBooksByTitle(String title)
  {
    return this.books.parallelStream()
      .filter(book -> title.equals(book.getTitle()))
      .collect(Collectors.toCollection(ArrayList::new));
  }


  /**
   * Temporary loading of books used for internal testing.
   */
  private void loadBooks()
  {
    this.addBook(new Book(
          "Harry Potter and the Philosopher's Stone", 
          "J. K. Rowling",
          "Bloomsbury Pub Ltd",
          2000,
          223,
          9780747532743L,
          true));

    this.addBook(new Book(
          "Harry Potter and the Chamber of Secrets", 
          "J. K. Rowling",
          "Bloomsbury Publishing PLC",
          1999,
          321,
          9780747538486L,
          true));

    this.addBook(new Book(
          "Legends: Discworld, Pern, Song of Ice and Fire, Memory, Sorrow and Thorn, Wheel of Time", 
          "Rebert Silverberg",
          "Harper Voyager",
          2011,
          197,
          9780006483946L,
          true));

    this.addBook(new Book(
          "A Knight of the Seven Kingdoms (Song of Ice & Fire Prequel)", 
          "Martin, George R.R.",
          "Harper Voyager",
          2017,
          423,
          9780008238094L,
          true));
  }
}
