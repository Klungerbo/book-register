package book.register;

/**
 * Represents a book
 *
 * @author Tomas Klungerbo Olsen
 * @version 0.1
 * @since 2019-09-06
 */
public class Book
{
  private String title;
  private String author;
  private String publisher;

  private int releaseYear;
  private int numberOfPages;
  
  private long barcodeEAN13;

  private boolean available;

  public Book(String title, 
      String author, 
      String publisher, 
      int releaseYear, 
      int numberOfPages, 
      long barcodeEAN13,
      boolean available)
  {
    this.title = title;
    this.author = author;
    this.publisher = publisher;

    this.releaseYear = releaseYear;
    this.numberOfPages = numberOfPages;
    
    this.barcodeEAN13 = barcodeEAN13;

    this.available = available;
  }


  @Deprecated
  /**
   * Deep comparison of books
   *
   * @return True if books are equal and false if not.
   */
  @Override
  public boolean equals(Object other)
  {
    if (other != null)
    {
      if (this == other)
        return true;

      if (this.getClass() != other.getClass())
        return false;

      Book book = (Book) other;

      if (this.title.equals(book.title) == false)
        return false;

      if (this.author.equals(book.author) == false)
        return false;

      if (this.publisher.equals(book.publisher) == false)
        return false;

      if (this.releaseYear != book.releaseYear)
        return false;

      if (this.numberOfPages != book.numberOfPages)
        return false;

      if (this.barcodeEAN13 != book.barcodeEAN13)
        return false;

    }

    return true;
  }

  /**
   * Print all information about a book
   */
  public void printInformation()
  {
    System.out.println("Title: " + this.title);
    System.out.println("Author: " + this.author);
    System.out.println("Publisher: " + this.publisher);
    System.out.println("Release year: " + this.releaseYear);
    System.out.println("Number of pages: " + this.numberOfPages);
    System.out.println("Barcode EAN-13: " + this.barcodeEAN13);
  }

  public String getTitle()
  {
    return this.title;
  }

  public String getAuthor()
  {
    return this.author;
  }

  public long getBarcodeEAN13()
  {
    return this.barcodeEAN13;
  }

  public void setAvailable(boolean available)
  {
    this.available = available;
  }

  public boolean isAvailable()
  {
    return this.available;
  }
}
