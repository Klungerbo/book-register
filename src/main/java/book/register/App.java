package book.register;

/**
 * The Application that is responsible for managing the book register.
 *
 * @author  Tomas Klungerbo Olsen
 * @version 0.1
 * @since   2019-09-06 
 */
public class App
{
  BookRegister bookRegister;

  public static void main(String[] args)
  {
    App app = new App();
    app.initialize();

    app.bookRegister.addBook(new Book("1984", 
        "George Orwell",
        "New American Library",
        1950,
        328,
        9780141036144L,
        true));

    app.bookRegister.addBook(new Book("1984", 
        "George Orwell",
        "New American Library",
        1950,
        328,
        9780141036144L,
        true));

    app.bookRegister.printAllBooks();

    app.bookRegister.removeBookByTitle("Harry Potter and the Chamber of Secrets");
    app.bookRegister.removeBookByTitle("1984");
  }

  private void initialize()
  {
    System.out.println("Initializing the book register...");
    System.out.println(""); 
    this.bookRegister = new BookRegister();
    this.bookRegister.initialize();
    System.out.println("Initializing complete"); 
    System.out.println(""); 
  }
}
